<a href="https://www.devops.cprime.com/" target="_blank">
<img src=".assets/logo.png" width="300" />
</a>

[![Maintained by CPrime Engineering](https://img.shields.io/badge/maintained%20by-cprime%20engineering-ED1846?style=flat-square)](https://www.devops.cprime.com/) 
[![Built for Engineers](https://img.shields.io/badge/project-devops%20library-ED1846?style=flat-square)](https://www.devops.cprime.com/library)
[![Terraform Version](https://img.shields.io/badge/terraform-=>%200.15.3-green?style=flat-square)](https://github.com/hashicorp/terraform) 
[![Hashicorp/AWS Version](https://img.shields.io/badge/hashicorp/aws-=>%203.39.0-green?style=flat-square)](https://github.com/hashicorp/terraform-provider-aws) 
[![Latest](https://img.shields.io/badge/latest-0.0.0-green?style=flat-square)](../../releases) 

# A Terraform Module from the CPrime Devops Library
This repository contains the [terraform](https://www.terraform.io/) configuration code that describes a Module from the CPrime DevOps Library.

![aws vpc](.assets/architecture.png)

## Quick start

[![Quick Start Video](/.assets/video-graphics.png)](https://www.youtube.com/watch?v=dQw4w9WgXcQ)

## How to use this repo

Follow these steps to fully implement this repo:

#### Step 0: GitLab.
If you are viewing this readme by any other means go to the canonical repository on [GitLab](https://Gitlab.com/cprime/skunkworks/shared/terraform-module-aws-vpc) for the most up to date version of this repo.

#### Step 1: Launch Gitpod IDE.
- In `https://gitlab.com/-/profile/preferences` ensure you have the Integrations - Enable Gitpod integration option checked.
- On the home page for this repo, to the left of the download button, there is a dropdown option. It will read 'Web IDE' or 'Gitpod.' Select 'Gitpod.'
- Then click on the selected 'Gitpod' option to launch the Gitpod Browser IDE (you can also right-click to open in a new tab).
- Gitpod will now launch.

#### Step 2: Bootstrap your IDE environment.
- From the command prompt in the project root of this repo, execute the following command.
- `sudo make ubuntu/bootstrap`

#### Step 3: Configure AWS.
- From the command prompt in the project root of this repo, execute the following command to configure your AWS credentials.
- `make awscli/configure AWS_SECRET_ACCESS_KEY=[your secret access key goes here] AWS_ACCESS_KEY_ID=[your access key ID goes here] AWS_REGION=eu-west-1 AWS_OUTPUT_FORMAT=json`

#### Step 4: Run the tests.
- From the command prompt in the project root of this repo, execute the following command to run the AWS integration tests.
- `make terraform/examples/complete/test`

#### Step 5: Eat, Sleep, Code, Repeat!
- Create a local working branch for whatever feature you are working on.
- Run local integration tests to ensure you won't push a breaking build.
- Push your branch to origin to trigger an automated CI build.
- If the automated CI build passed, create a merge request to have your code merged to the master branch.
- Rinse and repeat! Happy coding :)


## Dependencies

To deploy this repo and make use of the supporting code samples you will need the following dependencies installed on your developer workstation and CICD host:
- [Terraform](https://www.terraform.io/): Use Infrastructure as Code to provision and manage any cloud, infrastructure, or service.
- [AWS CLI](https://aws.amazon.com/cli/): The AWS Command Line Interface (CLI) is a unified tool to manage your AWS services.
- [Go](https://golang.org/): Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.

## Tooling

The following tools are not strictly required to work with this repo but we recommend their use and used them oursleves in the creation of this repo:
- [GitLab](https://Gitlab.com): GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and continuous integration and deployment pipeline features, using an open-source license, developed by GitLab Inc. 
- [Gitpod](https://www.gitpod.io/): Gitpod streamlines developer workflows by providing prebuilt, collaborative development environments in your browser - powered by VS Code.
- [CloudCraft](https://cloudcraft.co/): Create a professional architecture diagram in minutes with the Cloudcraft visual designer, optimized for AWS with smart components.

## What's included with this repo?

The structure of this repo follows the conventions described by [terraform.io](https://www.terraform.io/docs/language/modules/develop/structure.html).

This repo has the following folder structure:

- [test](/test): A suite of automated tests for this repo.
- [examples](/examples): Examples of how to use and implement the contents of this repo to deploy infrastructure as code.

## Who maintains this repo?

This repo is maintained by CPrime Engineering. If you're looking for support, send an email to [engineering@cprime.com](mailto:engineering@cprime.com?subject=DevOps%20Library%20VPC%20AWS).
CPrime Engineering can help with:

- Setup, customization, and support for this repo.
- Modules for other types of infrastructure, such as VPCs, Docker clusters, databases, and continuous integration.
- Modules that meet compliance requirements, such as HIPAA.
- Consulting & Training on AWS, Terraform, and DevOps.

## How do I contribute to this repo?

Contributions are welcome. Check out the
[Contribution Guidelines](/CONTRIBUTING.md) and 
[Code of Conduct](/CONDUCT.md) for instructions.

## How is this repo versioned?

This repo follows the principles of [Semantic Versioning](http://semver.org/). You can find each new release,
along with the changelog, in the [Releases Page](../../releases).

During initial development, the major version will be 0 (e.g., `0.x.y`), which indicates the code does not yet have a
stable API. Once we hit `1.0.0`, we will make every effort to maintain a backwards compatible API and use the MAJOR,
MINOR, and PATCH versions on each release to indicate any incompatibilities.

Publication of the CHANGELOG for this repo is automated using [git-changelog](https://github.com/git-chglog/git-chglog) in the style of [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Architectural Decisions

All architectural decisions made in the authoring of this repo are captured as a log of [Architecture Decision Records](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions) (ADRs). This log can be found in the [docs/en/adr](docs/en/adr) sub directory of this repo.

Creation of architecural decision records relating to this repo is automated using [adr-tools](https://github.com/npryce/adr-tools).

## License

This code is released under the Apache 2.0 License. Please see
[LICENSE](/LICENSE) and
[NOTICE](/NOTICE) for more details.

<a href="https://opensource.org/" target="_blank">
<img src=".assets/cp-osi-love.png" />
</a>
