#--------------------------------------------------------------------------------------------------------------
# Cprime label module
#--------------------------------------------------------------------------------------------------------------
module "label" {
  source  = "git::https://gitlab.com/cprime/devops-library/devops-library-terraform-module-label-null.git"

  context = module.this.context
}
